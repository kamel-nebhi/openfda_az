import csv
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('data/drug-label.csv', delimiter="\t", error_bad_lines=False)

df_result = df[["effective_time","spl_product_data_elements","route","manufacturer_name"]]
df_result['Date'] = pd.to_datetime(df['effective_time'],format="%Y%m%d",errors="coerce")
df_result.spl_product_data_elements = df_result.spl_product_data_elements.astype(str).str.replace('\[|\]|\'', '')
df_result['spl_product_data_elements_list'] = df_result.spl_product_data_elements.map(lambda x: [i.strip() for i in x.split(",")])
df_result['nb_ingredients'] = df_result.spl_product_data_elements_list.apply(len)

df_result.index = df_result['Date']
df_result.route = df_result.route.astype(str).str.replace('\[|\]|\'', '')

new_df = pd.DataFrame(data=df_result.route.str.split(',').tolist(), index=[df_result.Date,df_result.nb_ingredients]).stack()
new_df = new_df.reset_index([0, 'nb_ingredients'])
new_df.columns = ['Date','nb_ingredients','route']
new_df.route = new_df.route.str.strip()

yearly_summary = new_df[["Date","route","nb_ingredients"]]
yearly_summary.index = yearly_summary['Date']
del yearly_summary['Date']

# groupby is with a list - need to improve it to have unique value
grouped = yearly_summary.groupby(by='route').resample('Y')['nb_ingredients'].mean().fillna(0).reset_index()

print(grouped.head(n=100))

# plot the results for first 50 lines of df route types
df = grouped[:50].pivot(index='Date', columns='route', values='nb_ingredients')
df.plot()
plt.show()