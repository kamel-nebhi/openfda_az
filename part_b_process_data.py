import csv
import json
import pandas as pd
import os

# add all json files into the data dir
path_to_json = 'data/'
json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
print(json_files)

# access each json file for data processing
for index, js in enumerate(json_files):
    with open(os.path.join(path_to_json, js)) as json_file:
        data = json.load(json_file)

        df = pd.DataFrame(data["results"])
        df = pd.concat([df.drop(['openfda'], axis=1), df['openfda'].apply(pd.Series)], axis=1)
        df_temp = df[["effective_time", "spl_product_data_elements", "route", "manufacturer_name"]]

        # create new csv file for analysis
        if not os.path.isfile("data/drug-label.csv"):
            df_temp.to_csv("data/drug-label.csv", header=True,index=False,sep="\t", quoting=csv.QUOTE_NONE)
        else:  # else it exists so append without writing the header
            df_temp.to_csv("data/drug-label.csv", header=False, mode='a', index=False, sep="\t", quoting=csv.QUOTE_NONE)


