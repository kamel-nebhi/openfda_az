## part a

The script requests the openfda api and load the json results into a pandas
dataframe.
Then, there is different processing to plot the analysis.

## part b

The first script process the data as json files and transform it to an unique csv.

This csv is used in the second script to process the data and plot the results.