import requests
import json
import pprint
import pandas as pd
import matplotlib.pyplot as plt

pd.options.mode.chained_assignment = None  # default='warn'


# params for api query
params_suggest2 = {'search': 'openfda.manufacturer_name:"AstraZeneca"',
                   'limit': '100'
                   }

# api to query
# https://api.fda.gov/drug/label.json?search=openfda.manufacturer_name:%22AstraZeneca%22&limit=100
response = requests.get("https://api.fda.gov/drug/label.json", params=params_suggest2)

# receive data as json
data = response.json()

# transform json into pandas dataframe
df = pd.DataFrame(data["results"])

# extract values from openfda field
df = pd.concat([df.drop(['openfda'], axis=1), df['openfda'].apply(pd.Series)], axis=1)

df_result = df[["effective_time","spl_product_data_elements","manufacturer_name"]]
df_result['Date'] = pd.to_datetime(df['effective_time'],format="%Y%m%d")
df_result['spl_product_data_elements_list_str'] = df_result['spl_product_data_elements'].agg(lambda x: ','.join(map(str, x)))
df_result['spl_product_data_elements_list'] = df_result.spl_product_data_elements_list_str.map(lambda x: [i.strip() for i in x.split(",")])
df_result['nb_ingredients'] = df_result.spl_product_data_elements_list.apply(len)

# resampling dataframe by year
yearly_summary = df_result[["Date","nb_ingredients"]]
yearly_summary.index = yearly_summary['Date']
del yearly_summary['Date']

# plot summary
plt.rcParams['figure.figsize'] = (8, 6) # change plot size
yearly_summary.nb_ingredients.resample('Y').mean().fillna(0).plot(grid=True)
plt.title('Yearly # nb ingredients per drug')
plt.show()


